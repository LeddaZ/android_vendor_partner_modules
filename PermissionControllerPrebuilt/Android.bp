//
// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package {
    default_applicable_licenses: [
        "Android-Apache-2.0",
        "vendor_unbundled_google_modules_PermissionControllerPrebuilt_license",
    ],
}

license {
    name: "vendor_unbundled_google_modules_PermissionControllerPrebuilt_license",
    visibility: [":__subpackages__"],
    license_kinds: [
        "SPDX-license-identifier-BSD",
    ],
    license_text: ["LICENSE"],
}

soong_config_module_type_import {
    from: "packages/modules/common/Android.bp",
    module_types: ["module_apex_set"],
}

module_apex_set {
    name: "com.google.android.permission",
    apex_name: "com.android.permission",
    required: [
        "framework-permission.com.android.permission",
        "service-permission.com.android.permission",
    ],
    owner: "google",
    overrides: [
        "com.android.permission",
        "PermissionController",
        "com.android.permission.gms",
    ],
    filename: "com.google.android.permission.apex",
    set: "com.google.android.permission.apks",
    prefer: true,
    soong_config_variables: {
        module_build_from_source: {
            prefer: false,
        },
    },
    exported_bootclasspath_fragments: ["com.android.permission-bootclasspath-fragment"],
    exported_systemserverclasspath_fragments: ["com.android.permission-systemserverclasspath-fragment"],
}

module_apex_set {
    name: "com.google.android.permission_compressed",
    apex_name: "com.android.permission",
    required: [
        "framework-permission.com.android.permission",
        "service-permission.com.android.permission",
    ],
    owner: "google",
    overrides: [
        "com.android.permission",
        "PermissionController",
        "com.android.permission.gms",
    ],
//    filename: "com.google.android.permission.capex",
    set: "com.google.android.permission_compressed.apks",
    prefer: true,
    soong_config_variables: {
        module_build_from_source: {
            prefer: false,
        },
    },
    exported_bootclasspath_fragments: ["com.android.permission-bootclasspath-fragment"],
    exported_systemserverclasspath_fragments: ["com.android.permission-systemserverclasspath-fragment"],
}

prebuilt_etc {
    name: "GooglePermissionController_permissions.xml",
    src: "GooglePermissionController_permissions.xml",
    sub_dir: "permissions",
}

// for devices not supporting updatable APEX or low RAM devices
override_apex {
    name: "com.android.permission.gms",
    base: "com.android.permission",
    logging_parent: "com.google.android.modulemetadata",
    apps: [
        "GooglePermissionController",
        "SafetyCenterResources",
    ],
}

// non-updatable version that is included in com.android.permission.gms
android_app_set {
    name: "GooglePermissionController",
    owner: "google",
    set: "GooglePermissionController.apks",
    privileged: true,
    overrides: ["PermissionController",],
    required: ["GooglePermissionController_permissions.xml",],
}
