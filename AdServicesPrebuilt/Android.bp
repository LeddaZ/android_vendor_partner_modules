//
// Copyright (C) 2022 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package {
    default_applicable_licenses: [
        "Android-Apache-2.0",
        "vendor_unbundled_google_modules_AdServicesGooglePrebuilt_license",
    ],
}

// See: http://go/android-license-faq
license {
    name: "vendor_unbundled_google_modules_AdServicesGooglePrebuilt_license",
    visibility: [":__subpackages__"],
    license_kinds: [
        "SPDX-license-identifier-Apache-2.0",
        "SPDX-license-identifier-BSD",
        "SPDX-license-identifier-MIT",
        "SPDX-license-identifier-Unicode-DFS",
    ],
    license_text: ["LICENSE"],
}

soong_config_module_type_import {
    from: "packages/modules/common/Android.bp",
    module_types: ["module_apex_set"],
}

module_apex_set {
    name: "com.google.android.adservices",
    apex_name: "com.android.adservices",
    owner: "google",
    overrides: ["com.android.adservices"],
    filename: "com.google.android.adservices.apex",
    set: "com.google.android.adservices.apks",
    prefer: true,
    soong_config_variables: {
        module_build_from_source: {
            prefer: false,
        },
    },
    exported_bootclasspath_fragments: ["com.android.adservices-bootclasspath-fragment"],
    exported_systemserverclasspath_fragments: ["com.android.adservices-systemserverclasspath-fragment"],
}

module_apex_set {
    name: "com.google.android.adservices_compressed",
    apex_name: "com.android.adservices",
    owner: "google",
    overrides: ["com.android.adservices"],
//    filename: "com.google.android.adservices.capex",
    set: "com.google.android.adservices_compressed.apks",
    prefer: true,
    soong_config_variables: {
        module_build_from_source: {
            prefer: false,
        },
    },
    exported_bootclasspath_fragments: ["com.android.adservices-bootclasspath-fragment"],
    exported_systemserverclasspath_fragments: ["com.android.adservices-systemserverclasspath-fragment"],
}
